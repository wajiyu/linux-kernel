#include <linux/ksm.h>
#include <linux/hpa_rmap.h>

int hpa_page_mapcount(struct hugepage* page)
{
    return page_mapcount((struct page*)page);
}
EXPORT_SYMBOL(hpa_page_mapcount);

int hpa_page_mapcount_is_zero(struct hugepage* page)
{
    return !hpa_page_mapcount(page);
}
EXPORT_SYMBOL(hpa_page_mapcount_is_zero);

unsigned long hpa_pte_to_pfn(pte_t pte)
{
    return ((pte_val(pte) & ((1UL<<48)-1)) >> HUGEPAGE_SHIFT);
}
EXPORT_SYMBOL(hpa_pte_to_pfn);

static pte_t *__hpa_page_check_address(struct hugepage *page, struct mm_struct *mm,
                                       unsigned long address, spinlock_t **ptlp, int sync)
{
    pgd_t *pgd;
    pud_t *pud;
    pte_t *pte = NULL;
    spinlock_t *ptl;

    pgd = pgd_offset(mm, address);

    if(pgd_present(*pgd))
    {
        pud = pud_offset(pgd, address);
        if(pud_present(*pud))
        {
            pte = (pte_t *) pmd_offset(pud, address);
        }
    }

    if(!pte) return NULL;

    ptl = &mm->page_table_lock;

    spin_lock(ptl);

    if(pte_present(*pte) && hpa_page_to_pfn(page) == hpa_pte_to_pfn(*pte))
    {
        *ptlp = ptl;
        return pte;
    }

    pte_unmap_unlock(pte, ptl);
    return NULL;
}

pte_t *hpa_page_check_address(struct hugepage *page, struct mm_struct *mm,
                                            unsigned long address,
                                            spinlock_t **ptlp, int sync)
{
    pte_t *ptep;
    __cond_lock(*ptlp, ptep = __hpa_page_check_address(page, mm, address, ptlp, sync));
    return ptep;
}
EXPORT_SYMBOL(hpa_page_check_address);

static int hpa_try_to_unmap_one(struct page* page, struct vm_area_struct *vma,
                                unsigned long address, void *arg)
{
    struct mm_struct *mm = vma->vm_mm;
    pte_t *pte;
    pte_t pteval;
    spinlock_t *ptl;
    int ret = SWAP_AGAIN;
    enum ttu_flags flags = (enum ttu_flags)arg;

    //检查page有没有映射到此mm地址空间中
    //对ptl上锁
    pte = hpa_page_check_address((struct hugepage*)page, mm, address, &ptl, 0);

    if(!pte)
       goto out;

#ifdef NEED_VM_LOCKED
     //flags没有要求忽略mlock 的vam
    if(!(flags & TTU_IGNORE_MLOCK))
    {
        //此vma要求里面的页都锁在内存中
        if(vma->vm_flags & VM_LOCKED)
            goto out_mlock;

        //flags标记了对vma进行mlock释放模式
        if(flags & TTU_MUNLOCK)
            goto out_unmap;
    }
#endif
    //忽略页表项中的accessed标记
    if(!(flags & TTU_IGNORE_ACCESS))
    {
        //清除页表项的accessed标志
        if(ptep_clear_flush_young_notify(vma, address, pte))
        {
            ret = SWAP_FAIL;
            goto out_unmap;
        }
    }

     flush_cache_page(vma, address, hpa_page_to_pfn((struct hugepage*)page));

    //获取pte中的内容，并对pte清空
     pteval = ptep_clear_flush(vma, address, pte);

    //如果pte标记了此页为脏页，则设置page的PG_dirty标志位
     if(pte_dirty(pteval))
        set_page_dirty((struct page*)page);

     //更新进程所拥有的最大页框数
     update_hiwater_rss(mm);

     //对mm的文件页计数--
     //dec_mm_counter(mm,MM_FILEPAGES);
     //对此页的_mapcount--
     hpa_page_remove_rmap((struct hugepage*)page);

    //对此页的引用_count--
     page_cache_release((struct page*)page);

out_unmap:
     pte_unmap_unlock(pte, ptl);
     if(ret != SWAP_FAIL)
        mmu_notifier_invalidate_page(mm, address);
out:
    return ret;

#ifdef NEED_VM_LOCKED
out_mlock:
     pte_unmap_unlock(pte,ptl);

     if(down_read_trylock(&vma->vm_mm->mmap_sem))
     {
         if(vma->vm_flags & VM_LOCKED)
         {
             mlock_vma_page((struct page*)page);
             ret = SWAP_MLOCK;
         }
         up_read(&vma->vm_mm->mmap_sem);
     }
     return ret;
#endif
}

struct address_space *hpa_page_mapping(struct hugepage *page)
{
    return page_mapping((struct page*) page);
}
EXPORT_SYMBOL(hpa_page_mapping);

unsigned long hpa_vma_address(struct hugepage* page, struct vm_area_struct *vma)
{
    unsigned long address = vma->vm_start + ((page->index - vma->vm_pgoff)<<HUGEPAGE_SHIFT);
    return address;
}
EXPORT_SYMBOL(hpa_vma_address);

static inline int hpa_page_mapped(struct hugepage *page)
{
        return atomic_read(&(page)->_mapcount) >= 0;
}

static int hpa_page_not_mapped(struct page *page)
{
        return !hpa_page_mapped((struct hugepage*)page);
};

static int hpa_rmap_walk_file(struct hugepage *page, struct rmap_walk_control *rwc)
{
        struct address_space *mapping = page->mapping;
        pgoff_t pgoff;
        struct vm_area_struct *vma;
        int ret = SWAP_AGAIN;

        /*
         * The page lock not only makes sure that page->mapping cannot
         * suddenly be NULLified by truncation, it makes sure that the
         * structure at mapping cannot be freed and reused yet,
         * so we can safely take mapping->i_mmap_rwsem.
         */

        if (!mapping)
                return ret;

	pgoff = page->index;
        i_mmap_lock_read(mapping);
        vma_interval_tree_foreach(vma, &mapping->i_mmap, pgoff, pgoff) {
                unsigned long address = hpa_vma_address(page, vma);

                if (rwc->invalid_vma && rwc->invalid_vma(vma, rwc->arg))
                        continue;

                ret = rwc->rmap_one((struct page*)page, vma, address, rwc->arg);
                if (ret != SWAP_AGAIN)
                        goto done;
                if (rwc->done && rwc->done((struct page*)page))
                        goto done;
        }

done:
        i_mmap_unlock_read(mapping);
        return ret;
}

int hpa_rmap_walk(struct hugepage* page, struct rmap_walk_control *rwc)
{
        if (unlikely(PageKsm((struct page*)page)))
                return rmap_walk_ksm((struct page*)page, rwc);
        else
                return hpa_rmap_walk_file(page, rwc);
}

int hpa_try_to_unmap(struct hugepage *page, enum ttu_flags flags)
{
        int ret;
        struct rmap_walk_control rwc = {
                .rmap_one = hpa_try_to_unmap_one,
                .arg = (void *)flags,
                .done = hpa_page_not_mapped,
        };

	 /*
         * During exec, a temporary VMA is setup and later moved.
         * The VMA is moved under the anon_vma lock but not the
         * page tables leading to a race where migration cannot
         * find the migration ptes. Rather than increasing the
         * locking requirements of exec(), migration skips
         * temporary VMAs until after exec() completes.
         */

        ret = hpa_rmap_walk(page, &rwc);

        if (ret != SWAP_MLOCK && !hpa_page_mapped(page))
                ret = SWAP_SUCCESS;
        return ret;
}
EXPORT_SYMBOL(hpa_try_to_unmap);

static inline void *hpa_page_rmapping(struct hugepage *page)
{
        return (void *)((unsigned long)page->mapping & ~PAGE_MAPPING_FLAGS);
}


static int hpa_page_referenced_one(struct page *page, struct vm_area_struct *vma,
                        unsigned long address, void *arg)
{
        struct mm_struct *mm = vma->vm_mm;
        int referenced = 0;
        spinlock_t *ptl;
	struct page_referenced_arg *pra = arg;

	pte_t *pte;
        pte = hpa_page_check_address((struct hugepage *)page, mm, address, &ptl, 0);

        if (!pte)
            return SWAP_AGAIN;

        if (vma->vm_flags & VM_LOCKED) {
		pte_unmap_unlock(pte, ptl);
		pra->vm_flags |= VM_LOCKED;
            	return SWAP_FAIL;
        }

        if (ptep_clear_flush_young_notify(vma, address, pte)) {
             if (likely(!(vma->vm_flags & VM_SEQ_READ)))
                referenced++;
        }

        pte_unmap_unlock(pte, ptl);
	if(referenced) {
		pra->referenced++;
                pra->vm_flags |= vma->vm_flags;
        }

        pra->mapcount--;
        if (!pra->mapcount)
                return SWAP_SUCCESS; /* To break the loop */

        return SWAP_AGAIN;
}

int hpa_page_referenced(struct hugepage *page,
                    int is_locked,
                    struct mem_cgroup *memcg,
                    unsigned long *vm_flags)
{
        int ret;
        int we_locked = 0;
        struct page_referenced_arg pra = {
                .mapcount = hpa_page_mapcount(page),
                .memcg = memcg,
        };
        struct rmap_walk_control rwc = {
                .rmap_one = hpa_page_referenced_one,
                .arg = (void *)&pra,
        };

        *vm_flags = 0;
        if (!hpa_page_mapped(page))
                return 0;

        if (!hpa_page_rmapping(page))
                return 0;

        if (!is_locked && PageKsm((struct page*)page)) {
                we_locked = hpa_trylock_page(page);
                if (!we_locked)
                        return 1;
	}

        ret = hpa_rmap_walk(page, &rwc);
        *vm_flags = pra.vm_flags;

        if (we_locked)
                hpa_unlock_page(page);

        return pra.referenced;
}
EXPORT_SYMBOL(hpa_page_referenced);


void hpa_page_remove_rmap(struct hugepage *page)
{
        //bool locked;
        //unsigned long flags;

        //mem_cgroup_begin_update_page_stat(page, &locked, &flags);
	if (!atomic_add_negative(-1, &page->_mapcount))
                goto out;
        //__dec_zone_page_state(page, NR_FILE_MAPPED);
        //mem_cgroup_dec_page_stat(page, MEMCG_NR_FILE_MAPPED);
        //mem_cgroup_end_update_page_stat(page, &locked, &flags);
//	if (unlikely(PageMlocked((struct page*)page)))
//                clear_page_mlock((struct page*)page);
//        return;
out:
        //mem_cgroup_end_update_page_stat(page, &locked, &flags);
        return;
}
EXPORT_SYMBOL(hpa_page_remove_rmap);
