/*
 * Yet another hugepage allocator
 */

#include <linux/hpa.h>
#include <linux/mm.h>
#include <linux/atomic.h>
#include <linux/bootmem.h>
#include <linux/memblock.h>
#include "internal.h"

struct hugepage *huge_mem_map;
struct hpa_node *hpa_node_data;
struct hpa_section *hpa_section_array;

EXPORT_SYMBOL(huge_mem_map);
EXPORT_SYMBOL(hpa_node_data);
EXPORT_SYMBOL(hpa_section_array);
/*start pfn and number of whole hugepages area*/
unsigned long hpa_start_pfn;
unsigned long hpa_nr_pages;
unsigned long boundary_pfn;
unsigned long total_page;
unsigned long free_page;
EXPORT_SYMBOL(total_page);
EXPORT_SYMBOL(free_page);
EXPORT_SYMBOL(boundary_pfn);
EXPORT_SYMBOL(hpa_start_pfn);
EXPORT_SYMBOL(hpa_nr_pages);

void __hpa_free_page(struct hugepage *page)
{

	unsigned long flags;
	struct hpa_section *section;
	struct hpa_node *node;
	local_irq_save(flags);


	/*from struct hugepage to nid and section*/
	/*TODO for SMP add spin lock*/
	section = hpa_page_section(page);
	node = hpa_node_data;
	if (!section) {
		printk("Section don't be alloced!\n");
		return;
	}
	if (PageLRU((struct page*)page)) {
		__ClearPageLRU((struct page*)page);

		list_move(&page->lru,&section->free_list);
		if (PageActive((struct page *)page)) {
			node_page_state_add(-1, node, NR_ACTIVE_FILE);
		} else {
			node_page_state_add(-1, node, NR_INACTIVE_FILE);
		}
	} else {
                atomic_set(&page->_refcount,0);
		list_add(&page->lru,&section->free_list);
	}
	node_page_state_add(1, node, NR_FREE_PAGES);

	free_page++;
	local_irq_restore(flags);
}
EXPORT_SYMBOL(__hpa_free_page);


void hpa_free_page(struct hugepage *page)
{
	/* TODO some free page prepare */
	/*page refcount should be 1*/
	if (put_page_testzero((struct page*)page)) {
		/* turn off irq, but page fault will still happen*/
		__hpa_free_page(page);
	}
}
EXPORT_SYMBOL(hpa_free_page);

/*make sure page count is zero*/
void hpa_free_page_list(struct list_head *list)
{
	struct hugepage *page, *next;

	list_for_each_entry_safe(page, next, list, lru) {
		__hpa_free_page(page);
	}
}
EXPORT_SYMBOL(hpa_free_page_list);


static struct list_head *get_next_section_list(void)
{
	unsigned long nr_section = hpa_node_data->next_nr_section;
	unsigned long max_nr_section = hpa_node_data->node_max_sections;
	struct list_head *list;

	if ( nr_section + 2 >  max_nr_section )
		hpa_node_data->next_nr_section = 0;
	else
		hpa_node_data->next_nr_section++;

	list = &hpa_section_array[nr_section].free_list;
	return list;
}

struct hugepage *hpa_alloc_page_node(void)
{
	unsigned long flags;
	struct hugepage *page;
	struct list_head *list;
	unsigned long max_num, pnum;

	local_irq_save(flags);

	max_num = hpa_node_data->node_max_sections; 

	for (pnum = 0; pnum < max_num; pnum++) {

		list = get_next_section_list();


		if (list_empty(list))
			continue;
		else {
			page = list_first_entry(list, struct hugepage, lru);
			list_del(&page->lru);

			set_page_refcounted((struct page*)page);
			free_page--;
			local_irq_restore(flags);
			return page;
		}
	}

	/*failed*/
	local_irq_restore(flags);
	return NULL;
}
EXPORT_SYMBOL(hpa_alloc_page_node);

struct hugepage *hpa_alloc_page(void)
{
	struct hugepage *page;		
	
	page=hpa_alloc_page_node();
	if(page!=NULL) return page;		
	printk("Can't alloc more hugepage!\n");
	return NULL;
}
EXPORT_SYMBOL(hpa_alloc_page);


/* get_XXX function currently is fix-returned
 * we will calculate accordingly in the future
 * */
static unsigned long get_section_num(unsigned long nr_pages)
{
	unsigned long ret = (nr_pages + PAGE_PER_SECTION -1 ) >> PAGE_SECTION_SHIFT;
	//BUG_ON (ret != 1);
	return ret; 
}

static void __init hpa_alloc_section_node(void)
{

	unsigned long num_section;
	unsigned long pnum;
	struct hpa_section *section;
	/*allocation of section*/
	num_section = hpa_node_data->node_max_sections;
	if (num_section != 0) {
		section = alloc_bootmem(PAGE_SIZE);
		if (!section) {
			pr_err("Cannot find %zu bytes in node\n",
					PAGE_SIZE);
			return;
		}
		hpa_section_array = section;
		for (pnum=0; pnum < num_section; pnum++) {
			INIT_LIST_HEAD(&section[pnum].free_list);
		}
	}
	else
		hpa_section_array = 0;
}
/*
static void hpa_init_mem_mapping(void)
{
	unsigned long start,end;

	start = hpa_start_pfn << HUGEPAGE_SHIFT;
	end = (hpa_start_pfn + hpa_nr_pages) << HUGEPAGE_SHIFT;

	init_memory_mapping(start,end);

}
*/
static void hpa_memmap_init(unsigned long size,
		unsigned long start_pfn, unsigned long sectionid)
{
	unsigned long end_pfn = start_pfn + size;
	unsigned long pfn;


	for (pfn = start_pfn; pfn < end_pfn; pfn++)
	{
		struct hugepage *page = hpa_pfn_to_page(pfn);
		hpa_set_page_section(page,sectionid);
	}


}

/* according to function setup_node_data from arch/x86/mm/numa.c */
static void __init hpa_alloc_node_data(void)
{
	/*what is different between u64 and void star */
	const size_t nd_size = roundup(sizeof(struct hpa_node), PAGE_SIZE);
	//u64 nd_pa;
	void * nd;
	unsigned long size, start_pfn;
	unsigned long num_section;
	struct hpa_node *node = NULL;
	struct lruvec *lruvec;

	nd = alloc_bootmem(nd_size);
	//if (!nd_pa) {
	//    pr_err("Cannot find %zu bytes in node %d\n",
	//            nd_size, nid);
	//    return;
	//}
	//nd = __va(nd_pa);

	BUG_ON( nd == NULL);

	hpa_node_data = nd;
	node = hpa_node_data;
	lruvec = &node->lruvec;
	start_pfn = hpa_start_pfn;
	size = hpa_nr_pages;
	/*one section is 1G, num is got by being divided by nr_pages * 2M*/
	num_section = get_section_num(size);

	/*We need a macro HPA_NODE_DATA*/
	memset(node,0,sizeof(struct hpa_node));
	node->node_start_pfn = start_pfn;
	node->node_spanned_pages = size;
	node->node_present_pages = size;
	node->node_max_sections = num_section;
	node->next_nr_section = 0;

	lruvec->lists[LRU_INACTIVE_FILE].prev = &lruvec->lists[LRU_INACTIVE_FILE];
	lruvec->lists[LRU_INACTIVE_FILE].next = &lruvec->lists[LRU_INACTIVE_FILE];
	lruvec->lists[LRU_ACTIVE_FILE].prev = &lruvec->lists[LRU_ACTIVE_FILE];
	lruvec->lists[LRU_ACTIVE_FILE].next = &lruvec->lists[LRU_ACTIVE_FILE];
	node->pages_scanned = 0;
	node->watermark = 500;
	atomic_long_set(&node->vm_stat[NR_FREE_PAGES], 0);
	/*
	   INIT_LIST_HEAD(&node->section_list); 
	   */
	hpa_alloc_section_node();


	return;
}

static void __init hpa_nodes_init(void)
{
	/*TODO instead of possible map we should setup our own map*/
	unsigned long pnum, num_section;
	/*default section is 1G which is 1^9 hugepages*/
	unsigned long start_pfn = hpa_start_pfn, size = PAGE_PER_SECTION;
	hpa_alloc_node_data();
	num_section = hpa_node_data->node_max_sections;
	for( pnum = 0; pnum < num_section; pnum++, start_pfn+=size) {
		if ( pnum == num_section - 1 ) 
			size = hpa_start_pfn+hpa_nr_pages-start_pfn;
		hpa_memmap_init(size,start_pfn,pnum);
	}

	init_waitqueue_head(&hpa_node_data->waitq);

	//hpa_init_mem_mapping();
	return;
}

unsigned long hpa_free_all_boot_hugepages(unsigned long start_pfn, unsigned long end_pfn)
{
	unsigned long pfn;
	struct hugepage * page;
	for (pfn = start_pfn; pfn < end_pfn; pfn++) {
		page = hpa_pfn_to_page(pfn);
		atomic_set(&page->_mapcount, -1);
		set_page_refcounted((struct page*)page);
		hpa_free_page(page);
	}
	return 0;
}
EXPORT_SYMBOL(hpa_free_all_boot_hugepages);

static void boundary_init(void)
{
	boundary_pfn = hpa_start_pfn+hpa_nr_pages;
	printk("Boundary init at pfn %lu\n", boundary_pfn);
}

int __init hpa_init(void)
{
	int ret = 0;
	unsigned long size;
	total_page = 0;
 	free_page = 0;
	hpa_start_pfn = 512;
        hpa_nr_pages = 1024;

	/*start_pfn and size should be get dynamically */
	size = PAGE_ALIGN(sizeof(struct hugepage)* hpa_nr_pages);

	huge_mem_map = alloc_bootmem(size);

	hpa_nodes_init();

	//hpa_free_all_boot_hugepages(hpa_start_pfn, hpa_start_pfn+hpa_nr_pages);

	boundary_init();
	hpa_alloc_page();
	//boundary_change();

	return ret;
}
